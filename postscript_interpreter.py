#Christopher Roberts
#CptS:355
#HW4 postscript interprerter
import re
#global stacks
opstack = []
dictStack = []


def tokenize(s):
    return re.findall("/?[a-zA-Z][a-zA-Z0-9_]*|[[][a-zA-Z0-9_\s!][a-zA-Z0-9_\s!]*[]]|[-]?[0-9]+|[}{]+|%.*|[^ \t\n]",s)
# print (tokenize(
# """
#     [1 2 3 4 5] dup length /n exch def
#         /fact
#             {0 dict begin
#                 /n exch def
#                 n 2 lt
#                 { 1}
#                 {n 1 sub fact n mul }
#                 ifelse
#             end
#         } def
#     n fact stack
# """))

def opPop():
    if not opstack:
        return "nothing in the stack"
    else:
        result = opstack.pop()
        return result


def opPush(item):
    opstack.append(item)


def dictPop():
    if not dictStack:
        return "Stack has no value to pop"
    else:
        result = dictStack.pop()
        return result


#not much to do here because we only push empty curly braces to the dict stack
def dictPush():
    dictStack.append({})


#i tried to use def as a function name but it complained
def define(key, item):
    if not dictStack:
        dictStack.append({key:item})
        return
    dictStack[-1].update({key:item})


def lookup(key):
    if not dictStack:
        return False
    key = "/" + key
    for item in reversed(dictStack):
        if key in item.keys():
            return item[key]

#did not work like this :(
# def isNumber(item1, item2):
#     print(item1, item2)
#     if item1 is not int or not float:
#         print("false")
#         return False
#     if item2 is not int or not float:
#         print("false")
#         return False
#     print("true")
#     return True

def isNumber(item1, item2):
    if isinstance(item1, int) or isinstance(item1, float):
        if isinstance(item2, int) or isinstance(item2, float):
            return True
    return False


def add():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    if isNumber(item1, item2):
        opPush(item2 + item1)

def sub():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    if isNumber(item1, item2):
        opPush(item2 - item1)


def mul():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    if isNumber(item1, item2):
        opPush(item1 * item2)


def div():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()

    if isNumber(item1, item2):
        opPush(item2 / item1)


def eq():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    if isNumber(item1, item2):
        opPush(item1 is item2)
def lt():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()

    if isNumber(item1, item2):
        opPush(item1 > item2)


def gt():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()

    if isNumber(item1, item2):
        opPush(item1 < item2)


def length():
    if not opstack:
        return "opstack error: stack is empty"
    result = opPop()
    if isinstance(result, list):
        opPush(len(result))
    else:
        return "opstack error: top of stack is not a list"

def get():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    index = opPop()
    array = opPop()

    if not array:
        opPush(array)
        opPush(index)
        return "opstack error: array is empty"
    if not isinstance(index, int):
        opPush(array)
        opPush(index)
        return "opstack error: invalid arguments"
    if index > len(array) or index < 0:
        opPush(array)
        opPush(index)
        return "opstack error: index is out of range"
    opPush(array[index])


def psNot():
    if not opstack:
        return "opstack error: stack is empty"
    item1 = opPop()
    if not isinstance(item1, bool):
        opPush(item1)
        return "opstack error: expected bool argument"
    opPush(not item1)


def psAnd():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    if item1 is not bool or item2 is not bool:
        opPush(item2)
        opPush(item1)
        return "opstack error: invalid types, expected bool"
    opPush(item1 and item2)


def psOr():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    if not isinstance(item1, bool) or not isinstance(item1, bool):
        opPush(item2)
        opPush(item1)
        return "opstack error: expected bool values"
    opPush(item1 or item2)

def dup():
    if not opstack:
        return "opstack error: stack is empty"
    item1 = opPop()
    opPush(item1)
    opPush(item1)


def exch():
    if len(opstack) < 2:
        return "opstack error: expected 2 arguments"
    item1 = opPop()
    item2 = opPop()
    opPush(item1)
    opPush(item2)


def pop():
    if not opstack:
        return "opstack error: stack is empty"
    opPop()


def copy():
    if not opstack:
        return "opstack error: stack is empty"
    numItemsCopied = opPop()

    if numItemsCopied > len(opstack) or numItemsCopied < 1:
        opPush(numItemsCopied)
        return "opstack error: may not make more copies than items in stack or can not make 0 or less copies"

    copiedItems = []
    while numItemsCopied:
        item = opPop()
        copiedItems.append(item)
        numItemsCopied -= 1
    index = 0
    while index < 2:
        for item in reversed(copiedItems):
            opPush(item)
        index += 1


def clear():
    while opstack:
        opPop()


def stack():
    for item in reversed(opstack):
        print(item)


def psDict():
    opPop()
    emptyDict = {}
    opPush(emptyDict)


def begin():
    item = opPop()
    if not isinstance(item, dict):
        opPush(item)
        return "opstack error: top value must be a dictionary"
    dictPush()


def end():
    dictPop()


def psDef():
    item1 = opPop()
    item2 = opPop()
    define(item2, item1)


def psIf():
    item1 = opPop() #should be a code array
    item2 = opPop() #should be a bool
    if isinstance(item1, list): #can we preform list operations on item1?
        if isinstance(item2, bool): #is item2 a bool?
            if item2 == True: #should be preform this code block?
                interpretSPS(item1)
                return True
            else:
                return False
    opPush(item2)
    opPush(item1)

def ifelse():
    item1 = opPop() #shilud be a code array
    item2 = opPop() #should be a code array
    item3 = opPop() #should be a bool
    if isinstance(item1, list): #can we preform list operations on item1?
        if isinstance(item2, list): #is item2 a list?
            if isinstance(item3, bool): #is item3 a bool?
                if item3 == True: #if all of the conditions are met, which statement do we do?
                    interpretSPS(item2)
                else: #preform the else part of the ifelse
                    interpretSPS(item1)
                return
    opPush(item3)
    opPush(item2)
    opPush(item1)

def psFor():
    codeBlock = opPop()
    final = opPop()
    incr = opPop()
    init = opPop()

    #i don't know if i like cascading ifs better or if i like a list if ifs
    if not isinstance(codeBlock, list):
        opPush(init)
        opPush(incr)
        opPush(final)
        opPush(codeBlock)
    if not isinstance(final, int):
        opPush(init)
        opPush(incr)
        opPush(final)
        opPush(codeBlock)
    if not isinstance(incr, int):
        opPush(init)
        opPush(incr)
        opPush(final)
        opPush(codeBlock)
    if not isinstance(init, int):
        opPush(init)
        opPush(incr)
        opPush(final)
        opPush(codeBlock)

    while init != final:
        opPush(init)
        interpretSPS(codeBlock)
        init += incr
    if init == final: #we want to preform while init is final, so we do another if after the while loop
        opPush(init)
        interpretSPS(codeBlock)
        init += incr


def forall():
    operator = opPop()
    intArray = opPop()
    if isinstance(intArray, list):
        if isinstance(operator, list):
            for item in intArray:
                opPush(item)
                interpretSPS(operator)
            return
    opPush(intArray)
    opPush(operator)


def isOperator(item):
    if isinstance(item, str):
        if item == 'dup':
            return True
        elif item == 'mul':
            return True
        elif item == 'def':
            return True
        elif item == 'add':
            return True
        elif item == 'exch':
            return True
        elif item == 'pop':
            return True
        elif item == 'sub':
            return True
        elif item == 'div':
            return True
        elif item == 'eq':
            return True
        elif item == 'lt':
            return True
        elif item == 'gt':
            return True
        elif item == 'and':
            return True
        elif item == 'or':
            return True
        elif item == 'not':
            return True
        elif item == 'clear':
            return True
        elif item == 'stack':
            return True
        elif item == 'length':
            return True
        elif item == 'lookup':
            return True
        elif item == 'if':
            return True
        elif item == 'ifelse':
            return True
        elif item == 'forall':
            return True
    return False

def isValue(item):
    if lookup(item):
        return True
    else:
        return False


def groupMatching(it):
    res = []
    for c in it:
        if c== '}':
            return res
        else:
            res.append(groupMatching(it))


def group(s):
    res = []
    it = iter(s)
    for c in it:
        if c == '}':
            return False
        else:
            res.append(groupMatching(it))
    return res


def groupMatching2(it):
    res = []
    for c in it:
        if c == "}":
            return res
        elif c == "{":
            res.append(groupMatching2(it))
        elif c.isdigit():
            res.append(int(c))
        else:
            res.append(c)
    return False


def isIntArray(str):
    it = iter(str)
    for c in it:
        if c == " ":
            pass
        elif c == "[":
            pass
        elif c == "]":
            pass
        elif not c.isdigit():
            return False
    return True


def parse(L):
    res = []
    it = iter(L)
    for c in it:
        intArray = []
        if c == "}":
            return False
        elif c == "{":
            res.append(groupMatching2(it))
        elif c.isdigit():
            res.append(int(c))
        elif isIntArray(c):
            c = c.replace("[", '')
            c = c.replace("]", '')
            c = re.split(" ", c)
            for item in c:
                intArray.append(int(item))
            res.append(intArray)

        else:
            res.append(c)
    return res


def isCodeArray(L):
    result = False
    if isinstance(L, int):
        return False
    for element in L:
        if not isinstance(element, int):
            result = True
            break
    return result


#code array
def interpretSPS(code):
    it = iter(code)
    # print(code)
    for item in it:
        if isinstance(item, int):
            opPush(item)
        elif item == 'copy':
            copy()
        elif item == 'dup':
            dup()
        elif item == 'mul':
            mul()
        elif item == 'def':
            psDef()
        elif item == 'add':
            add()
        elif item == 'exch':
            exch()
        elif item == 'pop':
            pop()
        elif item == 'sub':
            sub()
        elif item == 'div':
            div()
        elif item == 'eq':
            eq()
        elif item == 'lt':
            lt()
        elif item == 'gt':
            gt()
        elif item == 'and':
            psAnd()
        elif item == 'or':
            psOr()
        elif item == 'not':
            psNot()
        elif item == 'clear':
            clear()
        elif item == 'stack':
            stack()
        elif item == 'length':
            length()
        elif item == 'lookup':
            lookup()
        elif item == 'if':
            psIf()
        elif item == 'ifelse':
            ifelse()
        elif item == 'forall':
            forall()
        elif item == 'for':
            psFor()
        elif item == 'begin':
            begin()
        elif item == 'end':
            end()
        elif item == 'dict':
            psDict()
        elif isinstance(item, bool):
            opPush(item)
        elif item == 'true':
            item = 'True'
            opPush(item)
        elif item == 'false':
            item = 'False'
            opPush(item)
        elif isinstance(item, list):
            opPush(item)
        elif isValue(item):
            value = lookup(item)
            if isinstance(value, list):
                interpretSPS(value)
            else:
                opPush(value)
        elif "-" in item:
            opPush(int(item))
        else:
            opPush(item)



# Copy this to your HW4_part2.py file>
def interpreter(s): # s is a string
    interpretSPS(parse(tokenize(s)))


#test cases copied from CptS355_Assign4_Part1Testcases.py

# ------- Part 1 TEST CASES--------------
def testDefine():
    define("/q3", 7)
    if lookup("q3") != 7:
        return False
    return True


def testLookup():
    opPush("/j4")
    opPush(1)
    psDef()
    if lookup("j4") != 1:
        return False
    return True


# Arithmatic operator tests
def testAdd():
    opPush(6)
    opPush(11)
    add()
    if opPop() != 17:
        return False
    return True


def testSub():
    opPush(15)
    opPush(4.5)
    sub()
    if opPop() != 10.5:
        return False
    return True


def testMul():
    opPush(5)
    opPush(2.2)
    mul()
    if opPop() != 11:
        return False
    return True


def testDiv():
    opPush(20)
    opPush(5)
    div()
    if opPop() != 4:
        return False
    return True


# Comparison operators tests
def testEq():
    opPush(7)
    opPush(7)
    eq()
    if opPop() != True:
        return False
    return True


def testLt():
    opPush(2)
    opPush(9)
    lt()
    if opPop() != True:
        return False
    return True


def testGt():
    opPush(1)
    opPush(11)
    gt()
    if opPop() != False:
        return False
    return True


# boolean operator tests
def testPsAnd():
    opPush(True)
    opPush(True)
    psAnd()
    if opPop() != True:
        return False
    return True


def testPsAnd2():
    opPush(False)
    opPush(False)
    psAnd()
    if opPop() != False:
        return False
    return True


def testPsOr():
    opPush(False)
    opPush(False)
    psOr()
    if opPop() != False:
        return False
    return True


def testPsNot():
    opPush(False)
    psNot()
    if opPop() != True:
        return False
    return True


# Array operator tests
def testLength():
    opPush([1, 2, 3, 4, 5, 6, 7, 8])
    length()
    if opPop() != 8:
        return False
    return True


def testGet():
    opPush([9, 7, 5, 3, 1])
    opPush(2)
    get()
    if opPop() != 5:
        return False
    return True


# stack manipulation functions
def testDup():
    opPush(15)
    dup()
    if opPop() != opPop():
        return False
    return True


def testExch():
    opPush(4)
    opPush("/m")
    exch()
    if opPop() != 4 and opPop() != "/m":
        return False
    return True


def testPop():
    l1 = len(opstack)
    opPush(42)
    opPush(10)
    pop()
    pop()
    l2 = len(opstack)
    if l1 != l2:
        return False
    return True


def testCopy():
    opPush(7)
    opPush(8)
    opPush(11)
    opPush(12)
    opPush(5)
    opPush(2)
    copy()
    if opPop() != 5 and opPop() != 12 and opPop() != 5 and opPop() != 12 and opPop() != 11 and opPop() != 8:
        return False
    return True


def testClear():
    opPush(42)
    opPush("/z")
    clear()
    stack()
    if len(opstack) != 0:
        return False
    return True


# dictionary stack operators
def testDict():
    opPush(8)
    psDict()
    if opPop() != {}:
        return False
    return True


def testBeginEnd():
    opPush("/q")
    opPush(5)
    psDef()
    opPush({})
    begin()
    opPush("/q")
    opPush(7)
    psDef()
    end()
    if lookup("q") != 5:
        return False
    return True


def testpsDef():
    opPush("/w")
    opPush(15)
    psDef()
    if lookup("w") != 15:
        return False
    return True


def testpsDef2():
    opPush("/e")
    opPush(22)
    psDef()
    opPush(1)
    psDict()
    begin()
    if lookup("e") != 22:
        end()
        return False
    end()
    return True

def testinterpretSPS1():

    while opstack:
        clear()

    # testing
    input1 = """
        /square {dup mul} def  
        [1 2 3 4] {square} forall 
        add add add 30 eq true 
        stack
    """
    interpreter(input1)
    value = opPop()
    if value == 'True':
        value = bool(value)
    if value != True:
        return False
    value = opPop()
    if value == 'True':
        value = bool(value)
    if value != True:
        return False
    if len(opstack) != 0:
        return False
    return True

def testinterpretSPS2():

    while opstack:
        clear()

    input2 = """ 
        [1 2 3 4 5] dup length /n exch def
        /fact {
            0 dict begin
                /n exch def
                n 2 lt
                { 1}
                {n 1 sub fact n mul }
                ifelse
            end 
        } def
      n fact stack    
    """
    interpreter(input2)
    if opPop() != 120:
        return False
    if opPop() != [1, 2, 3, 4, 5]:
        return False
    if len(opstack) != 0:
        return False
    return True

def testinterpretSPS3():

    while opstack:
        clear()
    result = False
    input3 = """
      [9 9 8 4 44 5 8 12 10 0 10 2 3 1 2 4 3] {dup 5 lt {pop} if}  forall 
      stack 
    """
    interpreter(input3)
    while opstack:
        value = opPop()
        if value < 5:
            return False
    return True

def testinterpretSPS4():

    while opstack:
        clear()

    input4 = """
      [1 2 3 4 5] dup length exch {dup mul}  forall
      add add add add
      exch 0 exch    -1 1 {dup mul add} for
      eq stack 
    """

    interpreter(input4)
    value = opPop()
    if value == 'True':
        value = bool(value)
    if value != True:
        return False
    return True

def testinterpretSPS5():

    while opstack:
        clear()
    input5 = """
    [10 9 8 7 6 5 4] {dup 8 gt {pop} if} forall
    stack
    """
    interpreter(input5)
    while opstack:
        value = opPop()
        if value > 8:
            return False
    return True

def testinterpretSPS6():
    while opstack:
        clear()

    input6 = """
    [1 2 3 4 5 6 7 8] {dup dup mul mul} forall add add add add add add add
    stack
    """
    interpreter(input6)
    while opstack:
        value = opPop()
        if value != 1296:
            return False
    return True

def testinterpretSPS7():
    while opstack:
        clear()

    input7 = """
    [1 2 3] length 5 lt {7 dup mul} {3 dup div} ifelse stack
    """
    interpreter(input7)
    while opstack:
        value = opPop()
        if value != 49:
            return False
    return True

def main_part1():
    testCases = [('define', testDefine), ('lookup', testLookup), ('add', testAdd), ('sub', testSub), ('mul', testMul),
                 ('div', testDiv), \
                 ('eq', testEq), ('lt', testLt), ('gt', testGt), ('psAnd', testPsAnd), ('psOr', testPsOr),
                 ('psNot', testPsNot), \
                 ('length', testLength), ('get', testGet), ('dup', testDup), ('exch', testExch), ('pop', testPop),
                 ('copy', testCopy), \
                 ('clear', testClear), ('dict', testDict), ('begin', testBeginEnd), ('psDef', testpsDef),
                 ('psDef2', testpsDef2), ('interpter1', testinterpretSPS1), ('interpretSPS2', testinterpretSPS2), ('interpretSPS3', testinterpretSPS3),
                 ('interpretSPS4', testinterpretSPS4), ('interpretSPS5', testinterpretSPS5), ('interpretSPS6', testinterpretSPS6), ('interpretSPS7', testinterpretSPS7)]
    # add you test functions to this list along with suitable names
    failedTests = [testName for (testName, testProc) in testCases if not testProc()]
    if failedTests:
        return ('Some tests failed', failedTests)
    else:
        return ('All part-1 and part-2 tests OK')


def main_part2():
    testcase5 = """
         1 2 3
         2 copy
         2 /x exch def
         div
         x
         add
         3 mul /y exch def
         pop
         y
         stack
        """
    # [1, 2, 8.0]
    print("----Test Case 5----")
    interpreter(testcase5)
    # output should be [1, 2, 8.0]
    clear()  # clear the stack for next test case


if __name__ == '__main__':
    print(main_part1())
