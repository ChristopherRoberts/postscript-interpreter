# PostScript Interpreter

PostScript Interpreter that reads postscript code as strings in python3 and executes accordingly.
I built this for a homework assignment. 
To run, just type python3 postscript_interpreter.py into the console.